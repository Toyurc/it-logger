import React, { useEffect } from 'react';
import { getTechs } from '../../actions/techActions';
import Preloader from '../layout/Preloader';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const TechSelectOptions = ({ tech: { techs, loading }, getTechs, onChange, selected }) => {

  // TEST 2: "Add Log Modal Select a Technician" 
  // 1.2. Complete the `TechSelectOptions` component to list all the technicians in a dropdown list.
  // First get the technicians, check if the endpoint has finished loading. 
  // Then map the technicians from state to create the options list with `firstName` and `lastName` values. 

  useEffect(() => {
    getTechs();
  }, [getTechs]);

  if (loading || techs.length <= 0) {
    return <Preloader />;
  }

  return (
    <div>
      <select
        data-testid='techSelectionDropdown'
        className='browser-default'
        value={selected}
        onChange={onChange}>
        <option disabled value={''}>Select Technician</option>
        {
          !loading && techs.length > 0 && techs.map((item) =>
            <option
              key={item.key}
              value={`${item.firstName} ${item.lastName}`}>
              {`${item.firstName} ${item.lastName}`}
            </option>
          )
        }
      </select>
    </div>)
};

TechSelectOptions.propTypes = {
  tech: PropTypes.object.isRequired,
  getTechs: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  selected: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  tech: state.tech,
});

export default connect(mapStateToProps, { getTechs })(TechSelectOptions);
