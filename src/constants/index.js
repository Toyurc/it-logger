import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

//Setup Mock Store
const mockStore = configureStore([thunk]);

export const Logs = [
  {
    "id": 1,
    "message": "Fixed hard drive on workstation 002",
    "tech": "Jennifer Williams",
    "attention": false,
    "date": "2023-02-12T02:52:34.384Z"
  },
  {
    "id": 2,
    "message": "New screen requested on workstation 002",
    "tech": "Sam Smith",
    "attention": false,
    "date": "2022-01-13T15:03:02.337Z"
  }];

export const Techs = [
  {
    "id": 1,
    "firstName": "John",
    "lastName": "Doe"
  },
  {
    "id": 2,
    "firstName": "Sam",
    "lastName": "Smith"
  },
  {
    "firstName": "Jennifer",
    "lastName": "Williams",
    "id": 4
  },
];

const mockRootStore = mockStore({
  log: {
    current:{
      id: 3,
      message: 'This is my test',
      tech: 'Jennifer Williams',
      date: new Date(),
      attention: true,
    },
    logs: Logs,
    loading: false,
  },
  tech: {
    techs: Techs,
    loading: false
  }
});

mockRootStore.dispatch = jest.fn()

export {mockRootStore}