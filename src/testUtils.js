import React from 'react'
import { render } from '@testing-library/react'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import rootReducer from './reducers';
import thunk from 'redux-thunk';

export const renderProvider = (
  ui,
  {
    preloadedState = {},
    store = createStore(
      rootReducer,
      preloadedState,
      applyMiddleware(thunk)
    ),
    ...renderOptions
  } = {}
) => {
  const  Wrapper = ({ children }) => {
    return <Provider store={store}>{children}</Provider>;
  }

  return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) };
}