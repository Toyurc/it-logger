import { rest } from 'msw'
import { Logs, Techs } from './constants';

//Setup Mock Request Handlers
export const handlers = [
  
  rest.get('/Logs', (req, res, ctx) => {
    return res(ctx.json(Logs), ctx.status(200))
  }),

  rest.delete('/logs/:logid', (req, res, ctx) => {
    return res(ctx.json(), ctx.status(200))
  }),

  rest.put('/logs/:log', (req, res, ctx) => {
    return res(ctx.json(Logs[1]), ctx.status(200))
  }),

  rest.post('/logs', (req, res, ctx) => {
    return res(ctx.json(Logs[0]), ctx.status(200))
  }),

  rest.get('/techs', (req, res, ctx) => {
    return res(ctx.json(Techs), ctx.status(200))
  })

];