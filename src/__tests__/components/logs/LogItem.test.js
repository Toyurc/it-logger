import React from 'react';
import { screen } from '@testing-library/react';
import moment from 'moment';
import LogItem from '../../../components/logs/LogItem';
import { renderProvider } from '../../../testUtils';

jest.mock('../../../actions/logActions');

describe('Single log item', () => {

  it('renders a logs item correctly', () => {
    
    let log =   {
      "id": 1,
      "message": "Fixed hard drive on workstation 002",
      "tech": "Toheeb Olorinla",
      "attention": true,
      "date": "2021-01-02T02:42:44.44Z"
    }

    renderProvider(<LogItem log={log} />)

    const time = moment(log.date).format('MMM Do YYYY, h:mm:ssa')
    expect(screen.getByText(time)).toBeInTheDocument();
    
    expect(screen.getByText(`ID #${log.id}`)).toBeInTheDocument();
    
    expect(screen.getByText(log.tech)).toBeInTheDocument();

    expect(screen.getAllByRole('link').at(0).innerHTML).toMatch(log.message);

  });

});