import React from 'react'
import { render, screen, within } from '@testing-library/react'
import { renderProvider } from '../../../testUtils'
import Logs from '../../../components/logs/Logs';
import moment from 'moment'
import { Provider } from 'react-redux'
import { mockRootStore as store } from '../../../constants';

describe('Logs Page', () => {

  it('renders Logs correctly', async () => {

    render(
      <Provider store={store}>
        <Logs />
      </Provider>
    );

    const logs = store.getState().log.logs

    const logItems = await screen.findAllByRole('listitem');

    logItems.shift()
    expect(logItems).toHaveLength(logs.length);

    logItems.forEach((logItem, index) => {

      expect(within(logItem).getAllByRole('link').at(0).innerHTML).toMatch(logs[index].message);

      const time = moment(logs[index].date).format('MMM Do YYYY, h:mm:ssa')
      expect(within(logItem).getByText(time)).toBeInTheDocument();

      expect(within(logItem).getByText(`ID #${logs[index].id}`)).toBeInTheDocument();

      expect(within(logItem).getByText(logs[index].tech)).toBeInTheDocument();

    });

  });

});