import React from 'react';
import { waitFor, screen } from '@testing-library/react';
import AddLogModal from '../../../components/logs/AddLogModal';
import { renderProvider } from '../../../testUtils';

jest.mock('../../../actions/logActions');

describe('Create new Log Modal', () => {

  it('renders new log form correctly', async () => {

   renderProvider(
      <AddLogModal />
    );

    expect(screen.getByText("Enter System Log")).toBeInTheDocument();

    expect(screen.getByTestId("addLogMessage")).toBeInTheDocument();

    await waitFor(() => expect(screen.getByTestId("techSelectionDropdown")).toBeInTheDocument());

    expect(screen.getByRole("checkbox", {name:"Needs Attention"})).toBeInTheDocument();

    expect(screen.getByRole("link", {name: "Enter"})).toBeInTheDocument();

  });
});