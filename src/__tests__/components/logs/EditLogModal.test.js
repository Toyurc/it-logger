import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import EditLogModal from '../../../components/logs/EditLogModal';
import { mockRootStore as store} from '../../../constants';

jest.mock('../../../actions/logActions');

describe('Edit Log Modal', () => {

  it('renders edit log form correctly', async () => {

    render(
      <Provider store={store}>
        <EditLogModal />
      </Provider>
    );

    const current = store.getState().log.current
    
    expect(screen.getByText("Edit System Log")).toBeInTheDocument()

    expect(screen.getByTestId("editMessage")).toHaveValue(current.message)
    
    expect(screen.getByTestId("techSelectionDropdown")).toBeInTheDocument()

  });

});