import React from 'react';
import { screen } from '@testing-library/react';
import { renderProvider } from '../../../testUtils'
import AddTechModal from '../../../components/techs/AddTechModal';

jest.mock('../../../actions/techActions.js');

describe('Create New Tech Modal', () => {

  it('renders add modal correctly', async () => {

   renderProvider(
      <AddTechModal />
    );

    expect(screen.getByText("New Technician")).toBeInTheDocument();

    expect(screen.getByTestId('firstName')).toBeInTheDocument();

    expect(screen.getByTestId('lastName')).toBeInTheDocument();

    expect(screen.getByRole("link", {name: "Enter"})).toBeInTheDocument();

  });

});